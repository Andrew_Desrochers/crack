#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * getname(char *prompt);

int main()
{
    char *fname;
    char *lname;
    
    fname = getname("What is your first name? ");
    lname = getname("What is your last name? ");
    printf("%s, %s\n", fname, lname);
    free(fname);
    free(lname);
}

char * getname(char *prompt)
{
    char *name = malloc(100);
    printf("%s", prompt);
    scanf("%s", name);
    return name;
}