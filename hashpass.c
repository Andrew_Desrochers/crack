#include "md5.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char *argv[])
{
    int catcher = 0;
    // Check number of cmd line args
    if (argc < 2)
    {
        printf("Must supply at least one filename\n");
        exit(1);
    }
    
    // Open first file for reading
    FILE *src = fopen(argv[1], "r");
    if (!src)
    {
        printf("can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    // Open second file for writing
    FILE *dest = fopen(argv[2], "w");
    if (!dest)
    {
        catcher = 1;
    }
    // Loop through first file, writing data to second file as we go
    char line[1000];
    while (fgets(line, 1000, src) != NULL)
    {
        int i = 0;
        char newline[1000];
        for(int i = 0; i < strlen(line); i++)
		{
            newline[i] = line[i];
        }
        char *hash = 0;
        hash = md5(newline, strlen(newline) - 1);
        
        if(catcher)
        {
            fprintf(stdout, "%s\n", hash);
        }
        else
        {
            fprintf(dest, "%s\n", hash);
        }
        free(hash);
    }
    fclose(src);
    fclose(dest);
}